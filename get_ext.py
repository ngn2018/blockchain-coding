import asterisk.config
import sys

# load and parse the config file
try:
   config = asterisk.config.Config('/etc/asterisk/extensions.conf')
except asterisk.config.ParseError as e:
   print "Parse Error line: %s: %s" % (e.line, e.strerror)
   sys.exit(1)
except IOError as e:
   print "Error opening file: %s" % e.strerror
   sys.exit(1)

# print our parsed output
for category in config.categories:
   if category.name == 'internal':
       print '[%s]' % category.name   # print the current category
       for item in category.items:
           print '   %s = %s' % (item.name, item.value)
